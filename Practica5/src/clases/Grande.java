package clases;

public class Grande extends Festivales {

	// Atributos

	private boolean parking;
	private boolean piscinas;

	// Constructores

	public Grande() {
		super();
		this.parking = false;
		this.piscinas = false;

	}

	public Grande(String codFestival, String nombre, String codComunidad, String nombreComunidad, int numeroArtistas,
			int numDias, String moneda, boolean parking, boolean piscinas) {
		super(codFestival, nombre, codComunidad, nombreComunidad, numeroArtistas, numDias, moneda);
		this.parking = parking;
		this.piscinas = piscinas;
	}

	
	public boolean isParking() {
		return parking;
	}

	public void setParking(boolean parking) {
		this.parking = parking;
	}

	public boolean isPiscinas() {
		return piscinas;
	}

	public void setPiscinas(boolean piscinas) {
		this.piscinas = piscinas;
	}

	@Override
	public String toString() {
		return super.toString() + "Grande [parking=" + parking + ", piscinas=" + piscinas + "]";
	}

	// Metodo para saber si el festival tiene parking

	public boolean festivalTiene(Grande festival3) {

		boolean parking;

		if (festival3.isParking() == true) {

			parking = true;

		} else {

			parking = false;
		}

		return parking;
	}

}
