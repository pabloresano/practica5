package clases;

public class Festivales {

	// Atributos

	private String codFestival;
	private String nombre;
	private String codComunidad;
	private String nombreComunidad;
	private int numeroArtistas;
	private int numDias;
	private String moneda;

	// Constructor

	public Festivales() {

		this.codFestival = "";
		this.nombre = "";
		this.codComunidad = "";
		this.nombreComunidad = "";
		this.numeroArtistas = 0;
		this.numDias = 0;
		this.moneda = "";
	}

	public Festivales(String codFestival, String nombre, String codComunidad, String nombreComunidad,
			int numeroArtistas, int numDias, String moneda) {

		this.codFestival = codFestival;
		this.nombre = nombre;
		this.codComunidad = codComunidad;
		this.nombreComunidad = nombreComunidad;
		this.numeroArtistas = numeroArtistas;
		this.numDias = numDias;
		this.moneda = moneda;
	}

	// Metodos get y setter

	public String getCodFestival() {
		return codFestival;
	}

	public void setCodFestival(String codFestival) {
		this.codFestival = codFestival;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodComunidad() {
		return codComunidad;
	}

	public void setCodComunidad(String codComunidad) {
		this.codComunidad = codComunidad;
	}

	public String getNombreComunidad() {
		return nombreComunidad;
	}

	public void setNombreComunidad(String nombreComunidad) {
		this.nombreComunidad = nombreComunidad;
	}

	public int getNumeroArtistas() {
		return numeroArtistas;
	}

	public void setNumeroArtistas(int numeroArtistas) {
		this.numeroArtistas = numeroArtistas;
	}

	public int getNumDias() {
		return numDias;
	}

	public void setNumDias(int numDias) {
		this.numDias = numDias;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	@Override
	public String toString() {
		return "Festivales [codFestival=" + codFestival + ", nombre=" + nombre + ", codComunidad=" + codComunidad
				+ ", nombreComunidad=" + nombreComunidad + ", numeroArtistas=" + numeroArtistas + ", numDias=" + numDias
				+ ", moneda=" + moneda + "]";
	}

	// Buscamos festivales que hay por comunidad

	public void listarFestivalesAtributo(Festivales festival1, String nombreComunidad) {

		if (festival1.getNombreComunidad().equals(nombreComunidad)) {

			System.out.println(festival1);
		}
	}

}
