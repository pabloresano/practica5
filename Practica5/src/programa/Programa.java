package programa;

import clases.Festivales;
import clases.Grande;
import clases.Peque�o;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Creo un festival");
		Festivales festival1 = new Festivales("A4", "STARLITE", "A1", "ANDALUCIA", 25, 3, "tokens");
		System.out.println("Codigo del festival: " + festival1.getCodFestival());
		System.out.println("Nombre del festival: " + festival1.getNombre());
		System.out.println("Codigo de la Comunidad: " + festival1.getCodComunidad());
		System.out.println("Nombre de la Comunidad: " + festival1.getNombreComunidad());
		System.out.println("Numero de artistas: " + festival1.getNumeroArtistas());
		System.out.println("Numero de dias: " + festival1.getNumDias());
		System.out.println("Moneda: " + festival1.getMoneda());
		
		System.out.println(" ");
		System.out.println("Listamos por nombre de Comunidad");
		festival1.listarFestivalesAtributo(festival1,festival1.getNombreComunidad());

System.out.println(" ");
		System.out.println("Creo un festival peque�o");
		Peque�o festival2 = new Peque�o("H1", "HOLIKA", "LR1", "LA RIOJA", 50, 5, "tokens", true, false);

		System.out.println("Codigo del festival: " + festival2.getCodFestival());
		System.out.println("Nombre del festival: " + festival2.getNombre());
		System.out.println("Codigo de la Comunidad: " + festival2.getCodComunidad());
		System.out.println("Nombre de la Comunidad: " + festival2.getNombreComunidad());
		System.out.println("Numero de artistas: " + festival2.getNumeroArtistas());
		System.out.println("Numero de dias: " + festival2.getNumDias());
		System.out.println("Moneda: " + festival2.getMoneda());

		if (festival2.isAtracciones() == true) {

			System.out.println(festival2.getNombre() + " tiene atracciones");
		} else {
			System.out.println(festival2.getNombre() + " no tiene atracciones");
		}

		if (festival2.festivalTiene(festival2)) {

			System.out.println(festival2.getNombre() + " tiene camping");
		} else {

			System.out.println(festival2.getNombre() + " no tiene camping");
		}
		System.out.println(" ");

		System.out.println("Creo un festival grande");
		Grande festival3 = new Grande("M1", "MEDUSA", "V1", "VALENCIA", 100, 7, "tokens", false, true);

		System.out.println("Codigo del festival: " + festival3.getCodFestival());
		System.out.println("Nombre del festival: " + festival3.getNombre());
		System.out.println("Codigo de la Comunidad: " + festival3.getCodComunidad());
		System.out.println("Nombre de la Comunidad: " + festival3.getNombreComunidad());
		System.out.println("Numero de artistas: " + festival3.getNumeroArtistas());
		System.out.println("Numero de dias: " + festival3.getNumDias());
		System.out.println("Moneda: " + festival3.getMoneda());
		if (festival3.isPiscinas() == true) {

			System.out.println(festival3.getNombre() + " tiene piscina");
		} else {
			System.out.println(festival3.getNombre()  + " no tiene piscina");
		}

		if (festival3.festivalTiene(festival3)==true) {

			System.out.println(festival3.getNombre() + " tiene parking");
		} else {

			System.out.println(festival3.getNombre()  + " no tiene parking");
		}

	}

}
