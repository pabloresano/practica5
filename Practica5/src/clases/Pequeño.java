package clases;

public class Peque�o extends Festivales {

	// Atributos

	private boolean atracciones = false;
	private boolean camping = false;

	// Constructores

	public Peque�o() {

		super();
		this.atracciones = false;
		this.camping = false;
	}

	public Peque�o(String codFestival, String nombre, String codComunidad, String nombreComunidad, int numeroArtistas,
			int numDias, String moneda, boolean atracciones, boolean camping) {

		super(codFestival, nombre, codComunidad, nombreComunidad, numeroArtistas, numDias, moneda);
		this.atracciones = atracciones;
		this.camping = camping;
	}

	// Metodos get y set

	public boolean isAtracciones() {
		return atracciones;
	}

	public void setAtracciones(boolean atracciones) {
		this.atracciones = atracciones;
	}

	public boolean isCamping() {
		return camping;
	}

	public void setCamping(boolean camping) {
		this.camping = camping;
	}

	// Metodo toString

	@Override
	public String toString() {
		return "Peque�o [atracciones=" + atracciones + ", camping=" + camping + ", festivales=";
	}

	// Metodo para saber si el festival tiene camping

	public boolean festivalTiene(Peque�o festival2) {

		boolean camping = false;

		if (festival2.isCamping() == true) {

			camping = true;

		} else {

			camping = false;
		}

		return camping;
	}

}
